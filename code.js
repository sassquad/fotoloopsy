(document).ready(function () {

    var searchField = $("#searchField"),
        pagerDiv = $("#viewMore"),
        container = $("#pics"),
        baseApi = "https://api.instagram.com/v1/",
        clientId = "client_id=a85d8ba662764a01969961a384bec2c2",
        timer;
    
    
        
    function loopsySearch(link) {
		var searchQuery = $(searchField).val();

        if (searchQuery.length > 0) {
            $.ajax({
                type: "GET",
                dataType: "jsonp",
                cache: false,
                url: link,
                success: function (feed) {
                    $.each(feed.data, function (i, item) {
                        var ncode = '<a href="' + feed.data[i].images.standard_resolution.url + '" target="_blank"><img src="' + feed.data[i].images.thumbnail.url + '" /></a>';
                        $(container).append(ncode);
                    });
                    if (feed.pagination.next_url) {
                        addNext(feed.pagination.next_url);
                    } else {
                        $(pagerDiv).empty();
                    }

                }
            });
        } else {
            loopsyRecent();
        }
    }
    
    function addNext(url) {
        $(pagerDiv).html('<a href="javascript:void(0);">View more</a>');
        //log($(pagerDiv));
        $(pagerDiv.selector + " a").on("click", function () {
            loopsySearch(url);
        });
    }
    
    function loopsyRecent() {
		$(container).empty();
        $(pagerDiv).empty();

        $.ajax({
            type: "GET",
            dataType: "jsonp",
            cache: false,
            url: "https://api.instagram.com/v1/media/popular?client_id=a85d8ba662764a01969961a384bec2c2",
            success: function (feed) {
                //log(feed);
                $(container).append("<h2>Most Recent Additions</h2>");

                $.each(feed.data, function (i, item) {
                    var ncode = '<a href="' + feed.data[i].images.standard_resolution.url + '" class="fullsize" target="_blank"><img src="' + feed.data[i].images.thumbnail.url + '" /></a>';
					$("#pics").append(ncode);
				});
                /*
                if (feed.pagination.next_url.length > 0) {
                    $(pagerDiv).html('<a href="'+feed.pagination.next_url+'">View more</a>');
                } else {
                    $(pagerDiv).empty();
                }
                */

            }
        });
    }
    
    $(searchField).on("keydown", function (e) {
        //if(e.keyCode == '32' || e.keyCode == '188' || e.keyCode == '189' || e.keyCode == '13' || e.keyCode == '190' || e.keyCode == '219' || e.keyCode == '221' || e.keyCode == '191' || e.keyCode == '220') {
            //e.preventDefault();
        //} else {
        clearTimeout(timer);
        var searchQuery = $(this).val();
        timer = setTimeout(function () {
            $(container).empty();
            $("#pics").append("<h2>Search Results</h2>");
            loopsySearch(baseApi + "tags/" + searchQuery + "/media/recent?" + clientId);
        }, 900);
        //})
    });
    
    
    if ($(searchField).val() === "") {
        loopsyRecent();
    }
    
    

});