# README #

This README details the Fotoloopsy application.

### What is this repository for? ###

* The Fotoloopsy application is a Javascript/jQuery Instagram feed. 
* You can view the latest additions to Instagram by default, or you can search by tag. 
* You can switch between grid and list layout via the accompanying radio buttons.
* Clicking on any image will take you to the Instagram page containing the full image (in a new tab)
* Version 1.0
* A live version is available at (http://www.sassquad.com/fotoloopsy)

### How do I get set up? ###

* The repo can be cloned, and saved to a folder e.g. fotoloopsy, of your local dev environment. To run it, simply visit http://localhost/fotoloopsy
* No configuration is required.
* Presently, the code relies on a CDN hosted version of jQuery, plus Google Fonts for use of the Roboto font. 

### Future improvements ###

* Further refactoring of JS - the functions for search and recent entries is largely the same, can be improved.
* open full size images inside modal dialog or lightbox

### Who do I talk to? ###

Stephen Scott (steve@sassquad.com) - visit my website at www.sassquad.com