$(document).ready(function () {

        // instantiate/initialise
        var searchField = $("#searchField"),
            pagerDiv    = $("#viewMore"),
            container   = $(".pics"),
            baseApi     = "https://api.instagram.com/v1/",
            clientId    = "client_id=a85d8ba662764a01969961a384bec2c2",
            titles      = ["Most Recent Additions","Search Results"],
            timer;
        
        /*
        
        caption.created_time
        caption.from.full_name
        caption.from.username
        comments.count
        likes.count
        
        */
        
        /* begin search query */    
        function loopsySearch(link) {
            var searchQuery = $(searchField).val();

            if (searchQuery.length > 0) {
                $(".title").text(titles[1] + " for '" + searchQuery + "'");
                $.ajax({
                    type: "GET",
                    dataType: "jsonp",
                    cache: false,
                    url: link,
                    success: function (feed) {
                        //console.log(feed);
                        $.each(feed.data, function (i, item) {
                            var date = new Date(feed.data[i].caption.created_time * 1000);
                            var itemMarkup = '<li><a href="' + feed.data[i].link + '" target="_blank"><img src="' + feed.data[i].images.thumbnail.url + '" /></a>';
                            itemMarkup+="<ul class='details'><li>" + feed.data[i].caption.from.username + "</li><li>" + date.getDate() +"/"+(date.getMonth()+1)+"/"+date.getFullYear()+" - " + feed.data[i].caption.text + "</li></ul>";
                            itemMarkup+="<ul class='stats'><li class='statsLikes'>" + feed.data[i].likes.count + "</li><li class='statsComments'>" + feed.data[i].comments.count + "</li></ul></li>";
                            $(container).append(itemMarkup);
                        });
                        if (feed.pagination.next_url) {
                            addNext(feed.pagination.next_url);
                        } else {
                            $(pagerDiv).empty();
                        }

                    }
                });
            } else {
                loopsyRecent();
            }
        }
        
        /* function to invoke next tranche of results via pagination (API has limit on requests) */
        function addNext(url) {
            $(pagerDiv).html('<a href="javascript:void(0);">View more</a>');
            $(pagerDiv.selector + " a").on("click", function () {
                loopsySearch(url);
            });
        }
        
        function loopsyRecent() {
            $(container).empty();
            $(pagerDiv).empty();
            $(".title").text(titles[0]);
            $.ajax({
                type: "GET",
                dataType: "jsonp",
                cache: false,
                url: baseApi + "media/popular?" + clientId,
                success: function (feed) {
                    $.each(feed.data, function (i, item) {                        
                        var date = (feed.data[i].created_time!=null ? new Date(feed.data[i].created_time * 1000) : "");
                        var itemMarkup = '<li><a href="' + feed.data[i].link + '" target="_blank"><img src="' + feed.data[i].images.thumbnail.url + '" /></a>';
                        itemMarkup+="<ul class='details'><li>" + feed.data[i].user.username + "</li><li>" + date.getDate() +"/"+(date.getMonth()+1)+"/"+date.getFullYear()+" - " + (feed.data[i].caption!=null ? feed.data[i].caption.text:"") + "</li></ul>";
                        itemMarkup+="<ul class='stats'><li class='statsLikes'>" + feed.data[i].likes.count + "</li><li class='statsComments'>" + feed.data[i].comments.count + "</li></ul></li>";
                        $(container).append(itemMarkup);
                    });               
                }
            });
        }
        
        $(searchField).on("keydown", function (e) {       
            clearTimeout(timer);
            var searchQuery = $(this).val();
            timer = setTimeout(function () {
                $(container).empty();
                loopsySearch(baseApi + "tags/" + searchQuery + "/media/recent?" + clientId);
            }, 900);
        });
        
        // radio button click
        $('input:radio[name=searchLayout]').on("click", function() {
            var sLayout = $('input:radio[name=searchLayout]:checked').val();
            if (sLayout === "list") {
                $(container).removeClass("grid").addClass("list");
            } else {
                $(container).removeClass("list").addClass("grid");
            }
        });
        
        // some initial screen setup
        function init() {
            var sLayout = $('input:radio[name=searchLayout]:checked').val();
            if (sLayout === "list") {
                $(container).removeClass("grid").addClass("list");
            } else {
                $(container).removeClass("list").addClass("grid");
            }
            
            if ($(searchField).val().length<1) {
                $(container).empty();
                $(pagerDiv).empty();
                $(".title").text(titles[0]);
                loopsyRecent();
            } else {
                $(".title").text(titles[0]);
            }
        }
        
        init();

    });